<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- bootstrap -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<!-- end bootstrap -->

</head>
<body>
	<div class ="container">
        <h1 style="text-align:center;">< Food Order ></h1> 
        <h1 style="text-align:center;"> <a href="<?echo base_url('backend/Master_data/food_menu_management');?>"> Backend </a></h1> 
        <hr>
         <?php foreach($menu_list as $row): ?>
         <div class="md-col-12">
         <div class="card">
    
                  <br>
                  <img style="width:100%;"
                  src="<? echo base_url('assets/uploads/food_image/'.$row['food_image'])?>" alt="">
                  <!-- <?= $row['food_image']; ?>  -->
         
         <div class="card-body" style="text-align:center; !important">
            <h5 class="card-title" style="text-align:center;"><?= $row['food_name']; ?> </h5>
            <!-- <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p> -->
            <a href="<? echo base_url('Api/order/'.$row['menu_id'])?>" class="btn btn-primary" style="width:100%;">Order</a>
        </div>
  
         </div>
         </div>
         <br>
         <?php endforeach; ?>
        
    </div>
    
    
            
	</div>
</body>
</html>
