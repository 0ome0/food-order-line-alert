<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- bootstrap -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<!-- end bootstrap -->

<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
</head>
<body>
	<div class ="container">
	<br>
		<a href='<?php echo site_url('backend/Master_data/food_menu_management')?>'>Food Menu</a> |
		<!-- <a href='<?php echo site_url('backend/Master_data/food_order_management')?>'>Orders</a> | -->
		<!-- <a href='#'>Orders</a> | -->
		<a href='<?php echo site_url('Menu_list')?>'>Front End</a> |
		<!-- <a href='<?php echo site_url('backend/Master_data/products_management')?>'>Products</a> |
		<a href='<?php echo site_url('backend/Master_data/offices_management')?>'>Offices</a> | 
		<a href='<?php echo site_url('backend/Master_data/employees_management')?>'>Employees</a> |		 
		<a href='<?php echo site_url('backend/Master_data/film_management')?>'>Films</a> |
		<a href='<?php echo site_url('backend/Master_data/multigrids')?>'>Multigrid [BETA]</a> -->
		
	</div>
	
	<div class ="container">
	<hr>
		<div style='height:20px;'></div>  
		<div style="padding: 10px">
			<?php echo $output; ?>
		</div>
		<?php foreach($js_files as $file): ?>
			<script src="<?php echo $file; ?>"></script>
		<?php endforeach; ?>
	</div>
</body>
</html>
