<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
    }
    
    public function order(){

        date_default_timezone_set("Asia/Bangkok");

        $menu_id = $this->uri->segment(3);
        
        // $menu_id = $this->input->get('menu_id');
        // $note = $this->input->get('note');
        
        // echo $menu_id;

        $sql = "select * from t_food_menu where menu_id = $menu_id";
        $menu_item= $this->db->query($sql);
        $data['menu_item'] = $menu_item->result_array();

        $food_name = $data['menu_item'][0]['food_name'];

        $Token = 'OH9tGfESRi8n8AG6LxWFUOh23rHpqKv2CDTiXZlTIM7';
        $message = "มี Order ใหม่ : $food_name ";

        // echo $message;

        $lineapi = $Token; // ใส่ token key ที่ได้มา
        $mms =  trim($message); // ข้อความที่ต้องการส่ง
        date_default_timezone_set("Asia/Bangkok");
        $chOne = curl_init(); 
        curl_setopt( $chOne, CURLOPT_URL, "https://notify-api.line.me/api/notify"); 
        // SSL USE 
        curl_setopt( $chOne, CURLOPT_SSL_VERIFYHOST, 0); 
        curl_setopt( $chOne, CURLOPT_SSL_VERIFYPEER, 0); 
        //POST 
        curl_setopt( $chOne, CURLOPT_POST, 1); 
        curl_setopt( $chOne, CURLOPT_POSTFIELDS, "message=$mms"); 
        curl_setopt( $chOne, CURLOPT_FOLLOWLOCATION, 1); 
        $headers = array( 'Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer '.$lineapi.'', );
            curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers); 
        curl_setopt( $chOne, CURLOPT_RETURNTRANSFER, 1); 
        $result = curl_exec( $chOne ); 
        //Check error 
        if(curl_error($chOne)) 
        { 
            echo 'error:' . curl_error($chOne); 
        } 
        else { 
        $result_ = json_decode($result, true); 
        // echo "status : ".$result_['status']; echo "message : ". $result_['message'];
        redirect('/menu_list/ordered','refresh');
            } 
        curl_close( $chOne );
    }
}
